# SportBetting

A simple app that covers all requirements.

For achieving result the following was used:
	
	- viewBinding
	
	- MVVM
	
	- Retrofit
	
Things that could've made it better:
	
	- DI - Dagger Hilt 
	
	- Unit testing - VMs with DI 
	
	- Favourites Persistance - Room/Realm or Local Storage
	
	- Better network handing - try again request option 
	
	- UI fine - tuning

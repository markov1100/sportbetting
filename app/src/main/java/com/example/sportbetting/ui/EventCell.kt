package com.example.sportbetting.ui

import android.content.Context
import android.os.CountDownTimer
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.example.sportbetting.R
import com.example.sportbetting.databinding.EventCellLayoutBinding
import java.util.Locale
import java.util.concurrent.TimeUnit

class EventCell @JvmOverloads constructor(
    ctx: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) :
    LinearLayout(ctx, attrs, defStyle) {

    companion object {
        private const val ONE_SECOND_MILLIS = 1000L
    }

    private var information: Information? = null

    private val binding: EventCellLayoutBinding =
        EventCellLayoutBinding.inflate(LayoutInflater.from(context), this, true)

    private var countDownTimer: CountDownTimer? = null

    data class Information(
        val id: Long,
        val sportCategory: String,
        val countdownTime: Long,
        val competitorOne: String,
        val competitorTwo: String,
        val isFavorite: Boolean
    )

    fun onFavouriteClick(onFavouriteClick: (Information) -> Unit) {
        binding.favouriteBtn.setOnClickListener {
            information?.let { info ->
                binding.favouriteBtn.setImageResource(
                    if (!information!!.isFavorite) R.drawable.favorite_full_ic
                    else R.drawable.favourited_empty_ic
                )
                information = info.copy(isFavorite = !info.isFavorite)
                onFavouriteClick(information!!)
            }
        }
    }

    fun setInformation(information: Information) {
        this.information = information

        binding.favouriteBtn.setImageResource(
            if (information.isFavorite) R.drawable.favorite_full_ic
            else R.drawable.favourited_empty_ic
        )

        binding.competitorOneTv.text = information.competitorOne
        binding.competitorTwoTv.text = information.competitorTwo
        startCountdown(information.countdownTime)
    }

    private fun startCountdown(timeInMillis: Long) {
        countDownTimer?.cancel()
        countDownTimer = object : CountDownTimer(timeInMillis, ONE_SECOND_MILLIS) {
            override fun onTick(millisUntilFinished: Long) {
                val timeRemaining = formatTime(millisUntilFinished)
                binding.countdownTimerTv.text = timeRemaining
            }

            override fun onFinish() {
                binding.countdownTimerTv.text = context.getString(
                    if (System.currentTimeMillis() - timeInMillis >= (90 * 60 * ONE_SECOND_MILLIS)) {
                        R.string.game_over
                    } else {
                        R.string.currently_playing
                    }
                )
                binding.countdownTimerTv.setSingleLine()
            }
        }.start()
    }

    private fun formatTime(millis: Long): String {
        val days = TimeUnit.MILLISECONDS.toDays(millis)

        val hours = TimeUnit.MILLISECONDS.toHours(millis) % 24
        val minutes = TimeUnit.MILLISECONDS.toMinutes(millis) % 60
        val seconds = TimeUnit.MILLISECONDS.toSeconds(millis) % 60

        val hoursMinutesSeconds =
            String.format(Locale.getDefault(), "%02d:%02d:%02d", hours, minutes, seconds)
        return if (days > 0) {
            String.format(Locale.getDefault(), "%d days %s", days, hoursMinutesSeconds)
        } else {
            hoursMinutesSeconds
        }
    }

}

package com.example.sportbetting.ui

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import android.widget.TextView
import com.example.sportbetting.R

class Toolbar @JvmOverloads constructor(ctx: Context, attrs: AttributeSet?, defStyle: Int = 0) :
    LinearLayout(ctx, attrs, defStyle) {

    private val title: TextView

    init {
        inflate(ctx, R.layout.toolbar_layout, this)
        title = findViewById(R.id.title_tv)
    }

}

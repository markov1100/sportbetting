package com.example.sportbetting.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.sportbetting.R
import com.example.sportbetting.databinding.SportCellLayoutBinding
import com.example.sportbetting.util.TimeUtil

class SportCell @JvmOverloads constructor(
    ctx: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) :
    LinearLayout(ctx, attrs, defStyle) {

    data class Information(
        val sportCategory: String,
        val games: List<EventCell.Information>
    )

    private class GameCellAdapter(
        val items: MutableList<EventCell.Information>,
        private val onFavouriteClickCallback: (EventCell.Information) -> Unit
    ) :
        RecyclerView.Adapter<GameCellAdapter.GameViewHolder>() {

        inner class GameViewHolder(private val gameCell: EventCell) :
            RecyclerView.ViewHolder(gameCell) {
            fun bind(game: EventCell.Information) {
                val timeDifference =
                    TimeUtil.convertToMillis(game.countdownTime) - System.currentTimeMillis()

                gameCell.setInformation(
                    EventCell.Information(
                        id = game.id,
                        sportCategory = game.sportCategory,
                        countdownTime = timeDifference,
                        competitorOne = game.competitorOne,
                        competitorTwo = game.competitorTwo,
                        isFavorite = game.isFavorite
                    )
                )

                gameCell.onFavouriteClick {
                    onFavouriteClickCallback(it)
                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameViewHolder {
            val gameCell = EventCell(parent.context)
            return GameViewHolder(gameCell)
        }

        override fun onBindViewHolder(holder: GameViewHolder, position: Int) {
            holder.bind(items[position])
        }

        override fun onBindViewHolder(
            holder: GameViewHolder,
            position: Int,
            payloads: MutableList<Any>
        ) {
            super.onBindViewHolder(holder, position, payloads)
        }

        override fun getItemCount(): Int {
            return items.size
        }

        fun updateList(newItems: List<EventCell.Information>) {
            val diffCallback = GameDiffCallback(items, newItems)
            val diffResult = DiffUtil.calculateDiff(diffCallback)
            items.clear()
            items.addAll(newItems)
            diffResult.dispatchUpdatesTo(this)
        }

        inner class GameDiffCallback(
            private val oldList: List<EventCell.Information>,
            private val newList: List<EventCell.Information>
        ) : DiffUtil.Callback() {
            override fun getOldListSize(): Int = oldList.size
            override fun getNewListSize(): Int = newList.size

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return oldList[oldItemPosition].id == newList[newItemPosition].id
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                oldList[oldItemPosition].sportCategory == newList[newItemPosition].sportCategory &&
                        oldList[oldItemPosition].competitorOne == newList[newItemPosition].competitorOne &&
                        oldList[oldItemPosition].competitorTwo == newList[newItemPosition].competitorTwo

        }
    }

    private val binding: SportCellLayoutBinding =
        SportCellLayoutBinding.inflate(LayoutInflater.from(context), this, true)

    private var sportEvents = listOf<EventCell.Information>()

    private var isCellCollapsed: Boolean = false

    init {
        setUpCollapseButton()
        setUpFavouritesToggle()
    }

    fun setSportInfo(information: Information) {
        binding.sportNameTv.text = information.sportCategory

        this.sportEvents = information.games.toMutableList()

        binding.gamesRv.adapter = GameCellAdapter(this.sportEvents.toMutableList()) { info ->
            this.sportEvents = this.sportEvents.map { item ->
                if (item.id == info.id) {
                    item.copy(isFavorite = info.isFavorite)
                } else {
                    item
                }
            }

            val result = if (binding.favouritesBtn.isChecked) {
                this.sportEvents.filter {
                    it.isFavorite
                }
            } else {
                this.sportEvents
            }

            if (result.isEmpty()) {
                binding.fillerView.isVisible = result.isEmpty()
                binding.gamesRv.isVisible = false
            }

            (binding.gamesRv.adapter as GameCellAdapter).updateList(result)
        }
    }

    private fun setUpCollapseButton() {
        binding.collapseBtn.setOnClickListener {
            if ((binding.gamesRv.adapter as GameCellAdapter).items.isEmpty()) {
                return@setOnClickListener
            }

            isCellCollapsed = binding.gamesRv.visibility != VISIBLE

            binding.gamesRv.isVisible = binding.gamesRv.visibility != VISIBLE
            binding.fillerView.isVisible = binding.gamesRv.visibility == GONE

            binding.collapseBtn.setImageResource(
                if (binding.gamesRv.visibility == VISIBLE) R.drawable.up_arrow_ic
                else R.drawable.down_arrow_ic
            )
        }
    }

    private fun setUpFavouritesToggle() {
        binding.favouritesBtn.setOnCheckedChangeListener { _, isChecked ->
            val filteredFavourites = if (isChecked) {
                sportEvents.filter {
                    it.isFavorite
                }
            } else {
                sportEvents
            }

            if (isCellCollapsed) {
                binding.gamesRv.isVisible = filteredFavourites.isEmpty().not()
                binding.fillerView.isVisible = filteredFavourites.isEmpty()
            }

            (binding.gamesRv.adapter as GameCellAdapter).updateList(filteredFavourites)
        }

    }
}

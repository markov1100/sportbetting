package com.example.sportbetting

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sportbetting.networking.RetrofitInstance
import com.example.sportbetting.networking.models.SportData
import com.example.sportbetting.ui.EventCell
import com.example.sportbetting.ui.SportCell
import com.example.sportbetting.util.StringUtils.extractMatchupNames
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response


class MainViewModel : ViewModel() {

    private val _sportData: MutableLiveData<List<SportCell.Information>> = MutableLiveData()

    val sportData: LiveData<List<SportCell.Information>>
        get() = _sportData

    sealed class LoadingStatus {
        data object Loading : LoadingStatus()
        data object Success : LoadingStatus()
        data object Error : LoadingStatus()
    }

    private val _loadingStatus: MutableLiveData<LoadingStatus> = MutableLiveData()

    val loadingStatus: LiveData<LoadingStatus>
        get() = _loadingStatus

    init {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingStatus.postValue(LoadingStatus.Loading)
            val sportBettingData: Response<List<SportData>>

            try {
                 sportBettingData = RetrofitInstance.api.getSportBettingData()
            } catch (e: Exception) {
                _loadingStatus.postValue(LoadingStatus.Error)
                return@launch
            }

            if (sportBettingData.isSuccessful.not()) {
                _loadingStatus.postValue(LoadingStatus.Error)
                return@launch
            }

            _sportData.postValue(sportBettingData.body()?.toList()?.map {
                SportCell.Information(sportCategory = it.sportName,
                    games = it.activeEvents.map { event ->
                        val teamsPlaying = extractMatchupNames(event.matchup)
                        EventCell.Information(
                            event.eventId.toLongOrNull() ?: -1L,
                            event.sportName,
                            event.startTime,
                            teamsPlaying.first,
                            teamsPlaying.second,
                            getFavouriteStatus(event.eventId.toLong())
                        )
                    })
            })

            _loadingStatus.postValue(LoadingStatus.Success)
        }
    }

    // Get from persistence
    private fun getFavouriteStatus(eventId: Long): Boolean {
        return false
    }

}


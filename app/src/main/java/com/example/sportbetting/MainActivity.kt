package com.example.sportbetting

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import com.example.sportbetting.MainViewModel.LoadingStatus
import com.example.sportbetting.databinding.ActivityMainBinding
import com.example.sportbetting.ui.SportCell

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(binding.root)

        binding.lifecycleOwner = this

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        viewModel.sportData.observe(this) { infoList ->
            binding.contentContainer.removeAllViews()
            infoList.forEach {
                binding.contentContainer.addView(SportCell(this).apply {
                    setSportInfo(it)
                })
            }
        }

        viewModel.loadingStatus.observe(this) { status ->
            when (status) {
                is LoadingStatus.Loading -> {
                    binding.spinnerContainer.isVisible = true
                }

                is LoadingStatus.Success -> {
                    binding.spinnerContainer.isVisible = false
                }

                is LoadingStatus.Error -> {
                    binding.spinnerContainer.isVisible = false
                    binding.errorMessage.isVisible = true
                }
            }
        }

    }
}

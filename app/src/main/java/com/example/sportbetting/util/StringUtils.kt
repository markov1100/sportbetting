package com.example.sportbetting.util

object StringUtils {
    fun extractMatchupNames(matchup: String): Pair<String, String> {
        val teams: List<String> = matchup.split("-")
        val team1 = teams[0].trim()
        val team2 = teams[1].trim()

        return Pair(team1, team2)
    }
}

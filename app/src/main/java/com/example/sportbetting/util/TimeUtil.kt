package com.example.sportbetting.util

object TimeUtil {
    // Api returns an example of 1710083220,
    // while System.currentTimeMillis() returns an example of 1716015695773
    // 1716015695773
    // 1710083220
    // to make sure the time is in milliseconds, we multiply by 1000
    fun convertToMillis(time: Long): Long {
        return if (time < 1_000_000_000_000) {
            time * 1000
        } else {
            time
        }
    }
}

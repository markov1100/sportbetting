package com.example.sportbetting.networking

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {
    val api: SportBettingApi by lazy {
        Retrofit.Builder()
            .baseUrl(SportBettingApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(SportBettingApi::class.java)
    }
}

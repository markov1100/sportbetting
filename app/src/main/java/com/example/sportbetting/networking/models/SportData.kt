package com.example.sportbetting.networking.models


import com.google.gson.annotations.SerializedName

data class SportData(
    @SerializedName("i")
    val sportId: String,
    @SerializedName("e")
    val activeEvents: List<SportEvent>,
    @SerializedName("d")
    val sportName: String,
)

package com.example.sportbetting.networking.models

import com.google.gson.annotations.SerializedName

data class SportEvent(
    @SerializedName("i")
    val eventId: String,
    @SerializedName("si")
    val sportName: String,
    @SerializedName("d")
    val matchup: String,
    @SerializedName("tt")
    val startTime: Long,
    @SerializedName("sh")
    val matchupCopy: String
)

package com.example.sportbetting.networking

import com.example.sportbetting.networking.models.SportData
import retrofit2.Response
import retrofit2.http.GET

interface SportBettingApi {

    companion object {
        const val BASE_URL = "https://618d3aa7fe09aa001744060a.mockapi.io"
        const val SPORTS_PATH = "/api/sports"
    }

    @GET(SPORTS_PATH)
    suspend fun getSportBettingData(): Response<List<SportData>>
}
